evolve
======


To run the example you need to download the following file:

    https://falonso.web.cern.ch/falonso/public/data.root

Use configuration file from example/example.conf as a template for your configfile and:

    evolve CONFIGFILE
